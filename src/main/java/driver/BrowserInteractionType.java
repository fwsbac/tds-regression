package driver;

/**
 * An enum defining the type of browser interaction that the test should utilize.
 *
 * Created by emunoz on 11/23/15.
 */
public enum BrowserInteractionType {
    MOUSE, KEYBOARD
}
