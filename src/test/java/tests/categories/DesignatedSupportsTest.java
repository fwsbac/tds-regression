package tests.categories;

/**
 * This interface defines a jUnit category for designated supports related tests.
 *
 * Created by emunoz on 1/20/16.
 */
public interface DesignatedSupportsTest {
}
