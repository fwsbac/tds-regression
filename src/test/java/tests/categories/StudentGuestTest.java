package tests.categories;

/**
 * This interface defines a jUnit category for tests executed with a guest student.
 *
 * Created by emunoz on 1/20/16.
 */
public interface StudentGuestTest {
}
